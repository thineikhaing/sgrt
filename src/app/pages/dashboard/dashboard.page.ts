import {Component, OnInit} from '@angular/core';
import {Geolocation, GeolocationOptions, Geoposition} from '@ionic-native/geolocation/ngx';
import {Platform, ToastController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {GoogleMap, GoogleMaps, GoogleMapsAnimation, GoogleMapsEvent, Marker, MyLocation} from '@ionic-native/google-maps/ngx';
import {NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult} from '@ionic-native/native-geocoder/ngx';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})


export class DashboardPage implements OnInit {
  options: GeolocationOptions
  map: GoogleMap
  loading: any
  currentPos: Geoposition
  isTracking = false
  geoAddress: String
  constructor(public toastCtrl: ToastController,
              private plt: Platform,
              private geolocation: Geolocation,
              private nativeGeocoder: NativeGeocoder,
              private storage: Storage) { }
  geocodeOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  }

  ngOnInit() {
    this.plt.ready();
    // this.getUserPosition();
    this.loadMap();
  }

  loadMap() {
    this.map = GoogleMaps.create('map', {
      camera: {
        target: {
          lat: 1.321888,
          lng: 103.8302616
        },
        zoom: 17
      }
    });
    console.log('Load google map event finished;');
    this.goToMyLocation();
  }

  goToMyLocation() {
    // Get the location of you
    this.map.getMyLocation().then((location: MyLocation) => {
      console.log(JSON.stringify(location, null , 2));
      this.map.clear()

      // this.getGeoencoder(location.latLng.lat, location.latLng.lng);

      // this.nativeGeocoder.reverseGeocode(location.latLng.lat, location.latLng.lng, this.geoOptions)
      //     .then((result: NativeGeocoderResult[]) => console.log('reverse geo coder result :::: ', JSON.stringify(result[0])))
      //     .catch((error: any) => console.log(error));

      // Move the map camera to the location with animation
      this.map.animateCamera({
        target: location.latLng,
        zoom: 17,
        duration: 3000
      });

      this.nativeGeocoder.reverseGeocode(location.latLng.lat, location.latLng.lng, this.geocodeOptions)
          .then((result: NativeGeocoderResult[]) => {
            // @ts-ignore
            this.geoAddress = this.generateAddress(result[0]);
            console.log(JSON.stringify(result[0]));

            const marker: Marker = this.map.addMarkerSync({
              title: 'You are here',
              snippet: this.generateAddress(result[0]),
              position: location.latLng,
              animation: GoogleMapsAnimation.BOUNCE
            });

            marker.showInfoWindow();
          })
          .catch((error: any) => {
            alert('Error getting location' + JSON.stringify(error));
          });
      // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
      //   this.showToast('clicked!');
      // });

      this.map.on(GoogleMapsEvent.MAP_READY).subscribe(
          (data) => {
            console.log('Click MAP', data);
          }
      );
    }).catch(err => {
          // this.loading.dismiss();
          this.showToast(err.error_message);
    });
  }

  generateAddress(addressObj) {
    console.log('Address object ')
    console.log(addressObj)
    const obj = [];
    let address = ''
    for (let key in addressObj) {
      if (key === 'thoroughfare' || key === 'countryName' || key === 'postalCode') {
        obj.push(addressObj[key]);
      }
    }
    obj.reverse();

    for (let val in obj) {

      if (obj[val].length) {
        address += obj[val] + ', ';
      }
    }
    console.log('Address ::: ' )
    console.log(address)
    return address;

  }

  async showToast(message: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }

  getUserPosition() {
    this.options = {
      enableHighAccuracy : true
    };

    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {

      this.currentPos = pos;
      console.log(pos);

    }, (err: PositionError) => {
      console.log('error : ' + err.message);
    });
  }

}
