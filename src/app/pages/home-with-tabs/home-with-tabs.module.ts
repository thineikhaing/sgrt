import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeWithTabsPage } from './home-with-tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: HomeWithTabsPage,
    children: [
      {
        path: 'dashboard',
        loadChildren: '../dashboard/dashboard.module#DashboardPageModule'
      },
      {
        path: 'dashboard/details',
        loadChildren: '../details/details.module#DetailsPageModule'
      },
      {
        path: 'topics',
        loadChildren: '../topics/topics.module#TopicsPageModule'
      },
      {
        path: 'bus-arrival',
        loadChildren: '../bus-arrival/bus-arrival.module#BusArrivalPageModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeWithTabsPage]
})
export class HomeWithTabsPageModule {}
