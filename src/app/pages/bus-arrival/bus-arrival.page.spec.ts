import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusArrivalPage } from './bus-arrival.page';

describe('BusArrivalPage', () => {
  let component: BusArrivalPage;
  let fixture: ComponentFixture<BusArrivalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusArrivalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusArrivalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
